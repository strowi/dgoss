FROM docker:stable

RUN mkdir -p /usr/local/bin \
  && apk --update --no-cache add \
    ca-certificates \
    curl \
    bash

# GOSS
# renovate: datasource=github-releases depName=aelsabbahy/goss versioning=loose
ENV GOSS_VERSION="v0.3.16"
RUN curl -L https://github.com/aelsabbahy/goss/releases/download/${GOSS_VERSION}/goss-linux-amd64 -o /usr/local/bin/goss \
  && curl -L https://github.com/aelsabbahy/goss/releases/download/${GOSS_VERSION}/dgoss -o /usr/local/bin/dgoss \
  && curl -L https://raw.githubusercontent.com/aelsabbahy/goss/${GOSS_VERSION}/extras/dcgoss/dcgoss -o /usr/local/bin/dcgoss \
  && chmod 0755 /usr/local/bin/*
