# Dgoss - local Installation

Mirrored from: <https://gitlab.com/strowi/dgoss>

## Docker Image

Should be available on docker and gitlab:

* [strowi/dgoss:latest](https://hub.docker.com/repository/docker/strowi/dgoss)
* [registry.gitlab.com/strowi/dgoss:latest](https://gitlab.com/strowi/dgoss)

## Install latest version to /usr/local/bin

```bash
curl -fsSL https://goss.rocks/install | sh
```

## Install v0.3.6 version to ~/bin

```bash
curl -fsSL https://goss.rocks/install | GOSS_VER=v0.3.6 GOSS_DST=~/bin sh
```

## Usage

For creating a container testfile run "dgoss edit" with normal docker parameters:

```bash
~> dgoss edit -e  BIND_PORT=:80 -e VCL_CONFIG=/etc/varnish/default.vcl -e CACHE_SIZE=1g -e VARNISHD_PARAMS="-p feature=+http2 -p default_ttl=120 -p default_grace=3600 -S /etc/varnish/secret -T 127.0.0.1:6082" hub.registry.net/sys/docker/varnish
~container> goss autoadd varnish
Adding Group to './goss.yaml':

varnish:
  exists: true
  gid: 101


Adding User to './goss.yaml':

varnish:
  exists: true
  uid: 101
  gid: 101
  groups:
  - varnish
  home: /home/varnish
  shell: /usr/sbin/nologin

~container> exit

INFO: Copied '/goss/goss.yaml' from container to '.'
INFO: Deleting container

~> dgoss run -e  BIND_PORT=:80 -e VCL_CONFIG=/etc/varnish/default.vcl -e CACHE_SIZE=1g -e VARNISHD_PARAMS="-p feature=+http2 -p default_ttl=120 -p default_grace=3600 -S /etc/varnish/secret -T 127.0.0.1:6082" hub.registry.net/sys/docker/varnish
 dgoss run -e  BIND_PORT=:80 -e VCL_CONFIG=/etc/varnish/default.vcl -e CACHE_SIZE=1g -e VARNISHD_PARAMS="-p feature=+http2 -p default_ttl=120 -p default_grace=3600 -S /etc/varnish/secret -T 127.0.0.1:6082" hub.registry.net/sys/docker/varnish
INFO: Starting docker container
INFO: Container ID: d71a4cef
INFO: Sleeping for 0.2
INFO: Container health
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                8288                8270                0                   09:59               ?                   00:00:00            s6-svscan -t0 /var/run/s6/services
root                8369                8288                0                   09:59               ?                   00:00:00            s6-supervise s6-fdholderd
root                8519                8288                0                   09:59               ?                   00:00:00            s6-supervise exporter
root                8520                8288                0                   09:59               ?                   00:00:00            s6-supervise varnish
root                8524                8520                0                   09:59               ?                   00:00:00            sh ./run
root                8531                8524                0                   09:59               ?                   00:00:00            /usr/sbin/varnishd -P /var/run/varnishd.pid -F -a :80 -f /etc/varnish/default.vcl -s malloc,1g -l 120m -p feature=+http2 -p default_ttl=120 -p default_grace=3600 -S /etc/varnish/secret -T 127.0.0.1:6082
INFO: Running Tests
Process: prometheus_varnish_exporter: running: matches expectation: [false]
Process: varnishd: running: matches expectation: [true]
User: varnish: exists: matches expectation: [true]
User: varnish: uid: matches expectation: [101]
User: varnish: gid: matches expectation: [101]
User: varnish: home: matches expectation: ["/home/varnish"]
User: varnish: groups: matches expectation: [["varnish"]]
User: varnish: shell: matches expectation: ["/usr/sbin/nologin"]
Port: tcp6:80: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp6:80: ip: skipped
Port: tcp:6082: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp:6082: ip: skipped
Port: tcp6:9131: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp6:9131: ip: skipped
Package: netbase: installed: matches expectation: [true]
Package: netbase: version: matches expectation: [["5.4"]]


Failures/Skipped:

Port: tcp6:80: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp6:80: ip: skipped

Port: tcp:6082: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp:6082: ip: skipped

Port: tcp6:9131: listening:
Expected
    <bool>: false
to equal
    <bool>: true
Port: tcp6:9131: ip: skipped

Total Duration: 0.009s
Count: 16, Failed: 3, Skipped: 3
INFO: Deleting container

```

alternative is to add services manually via 'goss add' inside the container.

## Gitlab-Pipeline 

```yaml
---
goss:
  stage: test
  image: hub.registry.net/sys/docker/test
  script:
    - echo "Logging to GitLab Container Registry with CI credentials..."
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - GOSS_FILES_STRATEGY=cp dgoss run $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG sleep 2s
```

